import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_regex/utils/estilos.dart';

class PaginaFiltragem extends StatefulWidget {
  const PaginaFiltragem({super.key});

  @override
  State<PaginaFiltragem> createState() => _PaginaFiltragemState();
}

class _PaginaFiltragemState extends State<PaginaFiltragem> {
  final controller1 = TextEditingController();
  final controller2 = TextEditingController();
  final controller3 = TextEditingController();
  final controller4 = TextEditingController();
  String textoFiltrado1 = '';
  String textoFiltrado2 = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Filtragem'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const Titulo(
                  texto:
                      'Aqui iremos demonstrar a filtragem em tempo real, nessa filtragem iremos remover apenas numeros da palavra digitada'),
              const SizedBox(height: 20),
              TextFormField(
                decoration: estiloForm,
                textAlign: TextAlign.center,
                controller: controller1,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onChanged: (value) {
                  setState(() {
                    textoFiltrado1 =
                        value.replaceAll(RegExp(r'[0-9]'), '').trim();
                  });
                },
              ),
              if (textoFiltrado1.isNotEmpty) const SizedBox(height: 20),
              Visibility(
                visible: textoFiltrado1.isNotEmpty,
                child: Legenda(
                  texto: textoFiltrado1,
                ),
              ),
              const SizedBox(height: 40),
              const Titulo(
                texto:
                    'Neste caso, temos um textfield que recebe um texto qualquer, ao receber o texto ele restringe o usuario de digitar numeros',
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                decoration: estiloForm,
                textAlign: TextAlign.center,
                controller: controller2,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                ],
              ),
              const SizedBox(height: 40),
              const Titulo(
                  texto:
                      'Exemplo para uma situaçao em que o usuario encaminha dados para uma API, podemos fazer a filtragem de dados antes do envio do texto'),
              const SizedBox(height: 40),
              TextFormField(
                decoration: estiloForm,
                textAlign: TextAlign.center,
                controller: controller3,
              ),
              TextButton(
                onPressed: () {
                  controller4.text =
                      controller3.text.replaceAll(RegExp(r'[0-9]'), '').trim();
                },
                child: const Text('Enviar'),
              ),
              const SizedBox(height: 20),
              const Center(
                child: Legenda(texto: 'Texto recebido pela API'),
              ),
              const SizedBox(height: 20),
              TextFormField(
                readOnly: true,
                decoration: estiloForm,
                controller: controller4,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
