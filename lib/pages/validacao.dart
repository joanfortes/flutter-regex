import 'package:flutter/material.dart';

import '../utils/estilos.dart';

class PaginaValidacao extends StatefulWidget {
  const PaginaValidacao({super.key});

  @override
  State<PaginaValidacao> createState() => _PaginaValidacaoState();
}

class _PaginaValidacaoState extends State<PaginaValidacao> {
  @override
  Widget build(BuildContext context) {
    final controller1 = TextEditingController();
    final controller2 = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Validação'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Titulo(
                texto: 'Identificador de caracteres indesejados',
              ),
              const SizedBox(height: 20),
              const Legenda(
                texto:
                    'Nesse caso o textField foi programado para fazer uma validação de senha se contém letra maiuscula, minuscula, numero, caractere especial e se tem 8 letras no minimo',
              ),
              const SizedBox(height: 30),
              TextFormField(
                decoration: estiloForm,
                controller: controller1,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (!RegExp(r'(?=.*?[A-Z])').hasMatch(value!)) {
                    return 'Precisa de ao menos 1 letra maiuscula';
                  }
                  if (!RegExp(r'(?=.*?[a-z])').hasMatch(value)) {
                    return 'Precisa de ao menos 1 letra minuscula';
                  }
                  if (!RegExp(r'(?=.*?[0-9])').hasMatch(value)) {
                    return 'Precisa de ao menos 1 número';
                  }
                  if (!RegExp(r'(?=.*?[#?!@$%^&*-])').hasMatch(value)) {
                    return 'Precisa de ao menos 1 caráctere especial';
                  }
                  if (!RegExp(r'.{8,}').hasMatch(value)) {
                    return 'Precisa de ao menos 8 digitos';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 40),
              const Legenda(
                texto:
                    'Nesse caso o textField foi programado para fazer uma validação de E-mail',
              ),
              const SizedBox(height: 30),
              TextFormField(
                decoration: estiloForm,
                controller: controller2,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (RegExp(r'^((?!\w+\@\w+\.[a-z]+\n*).)*$')
                      .hasMatch(value!)) {
                    return 'Este E-mail não é válido';
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
