import 'package:flutter/material.dart';

import '../utils/estilos.dart';

class PaginaContagem extends StatefulWidget {
  const PaginaContagem({super.key});

  @override
  State<PaginaContagem> createState() => _PaginaContagemState();
}

class _PaginaContagemState extends State<PaginaContagem> {
  int contagem = 0;
  String loremIpsum =
      ''' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec leo aliquam, vestibulum dui venenatis, tincidunt mauris. Donec mattis, sem sit amet posuere tincidunt, dolor mauris pulvinar quam, eu interdum ipsum justo consectetur nibh. Nulla est metus, luctus ac commodo vel, cursus eget tortor. Pellentesque malesuada lacus orci, a eleifend enim maximus at. Phasellus laoreet eros eget rutrum tristique. Fusce auctor at metus in pharetra. Sed ac metus condimentum, dapibus nunc in, lobortis erat. Etiam vel lorem sit amet metus semper pulvinar. Sed nec metus viverra tellus convallis hendrerit.''';
  final controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contagem'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Titulo(
                  texto:
                      'Nesse modo mostraremos capturas de palavras e contagem'),
              const SizedBox(height: 40),
              Legenda(texto: loremIpsum),
              const SizedBox(height: 40),
              TextFormField(
                decoration: estiloForm,
                controller: controller,
              ),
              const SizedBox(height: 20),
              TextButton(
                onPressed: () {
                  setState(() {
                    contagem = RegExp(r'\b' + controller.text + r'\b',
                            caseSensitive: false)
                        .allMatches(loremIpsum, 0)
                        .length;
                  });
                },
                child: const Legenda(texto: 'Contar Palavras'),
              ),
              const SizedBox(height: 20),
              Legenda(
                texto: controller.text == ''
                    ? 'Digite alguma coisa para buscar'
                    : contagem == 0
                        ? 'A palavra ${controller.text} não aparece no texto'
                        : 'Na frase acima a palavra ${controller.text} aparece $contagem vezes',
              )
            ],
          ),
        ),
      ),
    );
  }
}
