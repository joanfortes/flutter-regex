import 'package:flutter/material.dart';
import 'package:flutter_regex/pages/filtragem.dart';
import 'package:flutter_regex/pages/validacao.dart';
import 'package:flutter_regex/utils/estilos.dart';

import 'contagem.dart';
import 'mockagem.dart';

class PaginaInicial extends StatefulWidget {
  const PaginaInicial({super.key});

  @override
  State<PaginaInicial> createState() => _PaginaInicialState();
}

class _PaginaInicialState extends State<PaginaInicial> {
  final controller1 = TextEditingController();
  final controller2 = TextEditingController();
  final controller3 = TextEditingController();
  final controller4 = TextEditingController();
  String textoFiltrado1 = '';
  String textoFiltrado2 = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina Inicial'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const PaginaFiltragem(),
                  ),
                ),
                child: const Legenda(
                  texto: 'Tela de Filtragem',
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const PaginaValidacao(),
                  ),
                ),
                child: const Legenda(
                  texto: 'Tela de Validação',
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const PaginaMock(),
                  ),
                ),
                child: const Legenda(
                  texto: 'Tela de Mockagem',
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const PaginaContagem(),
                  ),
                ),
                child: const Legenda(
                  texto: 'Tela de Contagem',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
