import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utils/estilos.dart';

class PaginaMock extends StatefulWidget {
  const PaginaMock({super.key});

  @override
  State<PaginaMock> createState() => _PaginaMockState();
}

class _PaginaMockState extends State<PaginaMock> {
  final controller1 = TextEditingController();
  final controller2 = TextEditingController();
  final controller3 = TextEditingController();

  bool removerNumeros = false;
  bool removerLetras = false;
  bool removerCaracteresEspeciais = false;

  @override
  Widget build(BuildContext context) {
    CupertinoActionSheet buildActionSheet(BuildContext context) =>
        CupertinoActionSheet(
          actions: [
            CupertinoActionSheetAction(
                onPressed: () {
                  controller1.text = 'Re2mo4ve3r nume1ros';
                  setState(() {
                    removerNumeros = true;
                    removerCaracteresEspeciais = false;
                    removerLetras = false;
                  });
                  Navigator.pop(context);
                },
                child: const Text('Exemplo1')),
            CupertinoActionSheetAction(
                onPressed: () {
                  controller1.text = 'r0e1mov2er letr3a4s';
                  setState(() {
                    removerNumeros = false;
                    removerCaracteresEspeciais = false;
                    removerLetras = true;
                  });
                  Navigator.pop(context);
                },
                child: const Text('Exemplo2')),
            CupertinoActionSheetAction(
                onPressed: () {
                  controller1.text = r'$Re&mo.ve@r C?ar.ac-ter=es esp-ec#i@ais';
                  setState(() {
                    removerNumeros = false;
                    removerCaracteresEspeciais = true;
                    removerLetras = false;
                  });
                  Navigator.pop(context);
                },
                child: const Text('Exemplo3')),
          ],
        );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Mockagem'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Titulo(
                texto: 'Simulação de recebimento de dados via API',
              ),
              const SizedBox(height: 20),
              const Legenda(
                  texto:
                      'Nesse caso teremos 3 tipos de exemplos e neles iremos limpar as strings recebidas para posteriormente utilizarmos os dados recebidos'),
              const SizedBox(height: 40),
              TextFormField(
                decoration: estiloForm.copyWith(hintText: 'Selecione Exemplo'),
                controller: controller1,
                readOnly: true,
                textAlign: TextAlign.center,
                onTap: () {
                  showCupertinoModalPopup(
                      context: context, builder: buildActionSheet);
                },
              ),
              TextButton(
                onPressed: () {
                  if (removerCaracteresEspeciais) {
                    controller2.text = controller1.text
                        .replaceAll(RegExp(r'[^0-9a-zA-Z]'), '');
                  }
                  if (removerLetras) {
                    controller2.text =
                        controller1.text.replaceAll(RegExp(r'[a-zA-Z]'), '');
                  }
                  if (removerNumeros) {
                    controller2.text =
                        controller1.text.replaceAll(RegExp(r'[0-9]'), '');
                  }
                },
                child: const Text(
                  'Receber da API',
                ),
              ),
              TextFormField(
                readOnly: true,
                decoration: estiloForm.copyWith(hintText: 'Texto Formatado'),
                controller: controller2,
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
