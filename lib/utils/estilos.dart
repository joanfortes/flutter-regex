import 'package:flutter/material.dart';

OutlineInputBorder gerarBorda(Color color) {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(8),
    borderSide: BorderSide(
      color: color,
      width: 2,
    ),
  );
}

final estiloForm = InputDecoration(
  border: gerarBorda(Colors.black12),
  errorBorder: gerarBorda(Colors.red),
  enabledBorder: gerarBorda(Colors.black),
);

class Titulo extends StatelessWidget {
  final String texto;
  const Titulo({super.key, required this.texto});

  @override
  Widget build(BuildContext context) {
    return Text(
      texto,
      style: const TextStyle(
        fontSize: 22,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}

class Legenda extends StatelessWidget {
  final String texto;
  const Legenda({super.key, required this.texto});

  @override
  Widget build(BuildContext context) {
    return Text(
      texto,
      style: const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.normal,
      ),
    );
  }
}
